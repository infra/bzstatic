FROM python:3.9-alpine

RUN pip install --no-cache 'Flask==2.2.*' 'gunicorn==20.1.*'
WORKDIR /app
COPY app.py /app

ENTRYPOINT ["gunicorn"]
CMD ["-b", "0.0.0.0:5000", "app:app"]
