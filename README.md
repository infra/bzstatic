Scrapes all Xfce bug reports into static html files and their respective attachments.

### Fetching bugs
- Review bug and attachment max ID
- Review bugs and attachments blacklist
- Review message in `HEADER` of `dump.py`, `list.py` and `static/index.html` (specially the migration date)
- `pip install -r requirements.txt`
- `PYTHONUNBUFFERED=x python dump.py | tee log` # Takes about two hours
- `python list.py` # Should run in one minute or so
- Optional
  - Install minify: `yay -S minify-bin` or `sudo apt install minify`
  - `mkdir -p lists2 && minify lists -o lists2 && rm -r lists && mv lists2 lists`

### Serving
- Development
  - `python app.py`
- Production
  - `docker-compose up`
  - If nginx is used as reserve proxy, consider `sed -i 's/"5000:5000"/"127.0.0.1:5000:5000"/' docker-compose.yml`

### Screenshot

https://i.imgur.com/l8C8EXw.png
