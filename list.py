from bs4 import BeautifulSoup
from collections import defaultdict
from glob import glob
from pathlib import Path
import html
import multiprocessing

HEADER="""<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8">
<title>Xfce Bugzilla (Archive)</title>
<link rel="stylesheet" type="text/css" href="/style/bz.css">
</head><body class="bugzilla-xfce-org bz_bug">
<div class="readonly">! Please note that this is a snapshot of our old Bugzilla server, which is read only since May 29, 2020. Please go to <a href="https://gitlab.xfce.org" style="color:white;text-decoration:none;font-weight:bold;">gitlab.xfce.org</a> for our new server !</div>
<div id="bugzilla-body"><div id="main"><div id="content"><div id="article"><div id="page-index">"""
FOOTER='</div></div></div></div></div></body></html>'

TABLE_HEADER="""<table class="bz_buglist"><tbody>
<tr class="bz_buglist_header bz_first_buglist_header">
<th colspan="1">ID</th>
<th colspan="1">Product</th>
<th colspan="1">Component</th>
<th colspan="1">Status</th>
<th colspan="1">Resolution</th>
<th colspan="1">Summary</th>
<th colspan="1">Changed</th></tr>"""
TABLE_FOOTER='<tbody><table>'

Path("lists").mkdir(parents=True, exist_ok=True)

def read_bug(path):
    with open(path) as file:
        soup = BeautifulSoup(file, 'html.parser')

        id_ = int(soup.find_all(attrs={'name': 'id'})[0]['value'])
        product = str(soup.find(id='field_label_product').parent.find_all('span', 'select')[1].string)
        component = str(soup.find(id='field_label_component').parent.find_all('span', 'select')[0].string)
        status = str(soup.find(id='bug_status')['value'])
        resolution = str(soup.find(id='resolution')['value'])

        summary_tag = soup.find(id='summary_input')
        summary_span = summary_tag.find_all('span')
        summary = summary_span[0]['title'] if summary_span else summary_tag.string
        summary = html.escape(summary)

        last_modified = str(soup.find_all(class_='field-reportdates')[0].contents[-1])

        return [ id_, product, component, status, resolution, summary, last_modified ]

def generate_list(bugs):
    grouped_bugs = defaultdict(list)
    for b in bugs: grouped_bugs[b[1].strip()].append(b)

    for p in sorted(grouped_bugs):
        with open(f"lists/{p.lower().replace('.', '-')}.html", "w") as file:
            bug_count = len(grouped_bugs[p])
            bug_count = f"{bug_count} bug" if bug_count == 1 else f"{bug_count} bugs"
            file.write(HEADER + f"<h2>{p}</h2><p>{bug_count}</p>" + TABLE_HEADER)

            row_count = 0

            for b in sorted(grouped_bugs[p], key=lambda b: b[0]):
                row_count += 1
                file.write(f"""<tr class="bz_bugitem {'bz_row_even' if row_count % 2 == 0 else 'bz_row_odd'}">
                    <td class="bz_id_column"><a href="/show_bug.cgi?id={b[0]}">{b[0]}</a></td>
                    <td class="bz_product_column nowrap"><span class="no-color">{b[1]}</span></td>
                    <td class="bz_component_column nowrap"><span class="no-color">{b[2]}</span></td>
                    <td class="bz_bug_status_column nowrap"><span class="no-color">{b[3]}</span></td>
                    <td class="bz_resolution_column nowrap"><span class="no-color">{b[4]}</span></td>
                    <td class="bz_short_desc_column"><a href="/show_bug.cgi?id={b[0]}">{b[5]}</a></td>
                    <td class="bz_changeddate_column nowrap">{b[6]}</td></tr>""")

            file.write(TABLE_FOOTER + FOOTER)

print("Reading bugs...")

FILES=glob('./bugs/*.html')
with multiprocessing.Pool() as pool:
    BUGS = pool.map(read_bug, FILES)

print("Done. Writing list...")

generate_list(BUGS)
