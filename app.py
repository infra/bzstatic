from flask import Flask, request, send_file
from glob import glob

app = Flask(__name__, static_url_path='')

@app.route('/')
def index():
    return send_file('static/index.html')

@app.route('/list')
def buglist():
    return send_file('static/list.html')

@app.route('/list/<product>')
def product_list(product):
    return send_file(f'lists/{product}.html')

@app.route('/show_bug.cgi')
def bug():
    return send_file(f"bugs/{request.args['id']}.html")

@app.route('/attachment.cgi')
def attachment():
    files=glob(f"attachments/{request.args['id']}-#-*")
    if (len(files) == 0): abort(404)
    return send_file(files[0], download_name=files[0].split('-#-')[1], as_attachment=True)

if __name__ == '__main__':
    app.run()
