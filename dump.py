from bs4 import BeautifulSoup
from datetime import datetime
from joblib import Parallel, delayed
from pathlib import Path
from urllib.request import urlopen
from htmlmin import minify
import re

HEADER="""<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8">
<title>@/$TITLE@/$</title>
<link rel="stylesheet" type="text/css" href="/style/bz.css">
</head><body class="bugzilla-xfce-org bz_bug">
<div class="readonly">! Please note that this is a snapshot of our old Bugzilla server, which is read only since May 29, 2020. Please go to <a href="https://gitlab.xfce.org" style="color:white;text-decoration:none;font-weight:bold;">gitlab.xfce.org</a> for our new server !</div>"""
FOOTER='</body></html>'
MAX_BUG_ID=16891
MAX_ATTACHMENT_ID=9905

# Deleted bugs, spam, tests, nonsense...
BLACKLIST=[1120, 3358, 4354, 5093, 5468, 5571, 5572, 6077, 7988, 8908, 9110,
9123, 9133, 10750, 11299, 11890, 12321, 12327, 12590, 12591, 12592, 12593,
12594, 12595, 12596, 12597, 12598, 12599, 12600, 12623, 12624, 12856, 12896,
12920, 12921, 12922, 12976, 12977, 12978, 12979, 12980, 12981, 12982, 12983,
12984, 12985, 12986, 12987, 12988, 12989, 12990, 12991, 12992, 12993, 12994,
12995, 12996, 12997, 12998, 12999, 13000, 13001, 13002, 13004, 13005, 13006,
13007, 13008, 13009, 13010, 13011, 13012, 13013, 13014, 13015, 13016, 13018,
13019, 13020, 13021, 13022, 13023, 13024, 13025, 13026, 13027, 13028, 13029,
13030, 13031, 13033, 13034, 13035, 13036, 13037, 13038, 13039, 13040, 13041,
13042, 13043, 13044, 13045, 13046, 13047, 13048, 13049, 13050, 13052, 13056,
13057, 13058, 13059, 13060, 13061, 13062, 13063, 13064, 13065, 13066, 13067,
13068, 13069, 13070, 13071, 13072, 13073, 13074, 13075, 13078, 13079, 13080,
13081, 13082, 13083, 13084, 13085, 13086, 13087, 13088, 13089, 13090, 13091,
13092, 13093, 13094, 13095, 13096, 13097, 13098, 13099, 13100, 13101, 13103,
13104, 13105, 13106, 13107, 13109, 13110, 13111, 13112, 13113, 13115, 13116,
13117, 13118, 13119, 13120, 13121, 13122, 13123, 13124, 13125, 13126, 13127,
13128, 13129, 13130, 13131, 13132, 13133, 13134, 13135, 13136, 13137, 13138,
13140, 13141, 13142, 13143, 13144, 13145, 13146, 13147, 13148, 13149, 13150,
13151, 13152, 13153, 13154, 13155, 13156, 13157, 13158, 13159, 13160, 13161,
13162, 13163, 13164, 13165, 13166, 13167, 13170, 13171, 13172, 13173, 13174,
13175, 13176, 13177, 13178, 13179, 13180, 13181, 13182, 13183, 13184, 13185,
13186, 13187, 13188, 13189, 13190, 13191, 13192, 13193, 13194, 13195, 13196,
13197, 13198, 13199, 13232, 13233, 13234, 13235, 13245, 13247, 13276, 13277,
13377, 13390, 13503, 13596, 13688, 13733, 13734, 13735, 13736, 13739, 13740,
13741, 13771, 13772, 13773, 13774, 13775, 13776, 13777, 13778, 13793, 13794,
13795, 13808, 13834, 13846, 13939, 14053, 14111, 14112, 14149, 14150, 14151,
14152, 14153, 14200, 14204, 14212, 14223, 14270, 14874, 15101, 16043, 16074,
16637, 16757]

# From the bugs above
BLACKLIST_ATTACHMENT=[760, 998, 999, 1085, 1172, 1173, 1192, 1193, 1194, 1195,
1196, 1197, 1198, 1199, 1200, 1201, 1202, 1203, 1204, 1205, 1206, 1207, 1208,
1209, 1210, 1211, 1212, 1213, 1214, 1215, 1216, 1217, 1218, 1219, 1220, 1221,
1222, 1223, 1224, 1225, 1226, 1227, 1228, 1229, 1230, 1231, 1232, 1233, 1234,
1235, 1236, 1237, 1238, 1239, 1240, 1241, 1242, 1243, 1244, 1245, 1246, 1247,
1248, 1249, 1250, 1251, 1252, 1253, 1254, 1255, 1256, 1257, 1258, 1259, 1260,
1261, 1262, 1263, 1264, 1265, 1266, 1267, 1268, 1355, 1682, 1683, 1684, 1685,
1870, 1874, 1922, 1993, 2057, 2111, 2112, 2733, 4547, 6071, 6232, 6538, 6845,
6880, 6923, 6924, 6925, 6926, 7193, 7238, 7240, 7255, 7256, 7257, 7259, 7260,
7261, 7262, 7264, 7274, 7275, 7276, 7292, 7305, 7462, 7505, 7601, 8481, 8482,
8483, 9115, 9674]

for p in ["attachments", "bugs", "static/style", "static/images"]:
    Path(p).mkdir(parents=True, exist_ok=True)

def fetch_bug(id):
    try:
        if id in BLACKLIST:
            print(f"Skipping bug #{id}...")
            return

        print(f"Fetching bug #{id}...")

        response = urlopen(f"https://bugzilla.xfce.org/show_bug.cgi?id={id}")
        if response.status != 200:
            print(f"Failed to fetch bug {id}: {response.status}")
            return
        data = response.read()

        soup = BeautifulSoup(data.decode('utf-8'), 'html.parser')
        title = soup.title.string
        body = soup.find(id="bugzilla-body")

        body.find(id="summary_container").decompose()
        body.find(id="add_comment").decompose()

        button = body.find(id="show_dependency_tree_or_graph")
        if button: button.decompose()

        body.find(class_="bz_attach_footer").decompose()

        for el in body.find_all(class_="action-link"): el.decompose()
        for el in body.find_all("script"): el.decompose()
        for el in body.find_all("a", text=re.compile("\[?[Dd]etails\]?")): el.decompose()
        for el in body.find_all(class_="field_help_link"): el['href'] = '#'

        html = minify(HEADER.replace('@/$TITLE@/$', title) + body.prettify() + FOOTER, remove_optional_attribute_quotes=False, remove_comments=True, reduce_boolean_attributes=True)

        with open(f"bugs/{id}.html", "w") as file:
            file.write(html)
    except:
        print(f"Failed to fetch bug #{id}!")

def fetch(url, dest, append=False):
    response = urlopen(url)
    if response.status != 200:
        print(f"Failed to fetch {url}: {response.status}")
        return

    with open(dest, "ab" if append else "wb") as file:
        file.write(response.read())

def fetch_attachment(id):
    if id in BLACKLIST_ATTACHMENT:
        print(f"Skipping attachment #{id}...")
        return

    print (f"Fetching attachment #{id}...")

    response = urlopen(f"https://bugzilla.xfce.org/attachment.cgi?id={id}")
    if response.status != 200:
        print(f"Failed to attachment {id}: {response.status}")
        return

    try:
        name = re.search('name="(.+)"', response.headers['Content-Type']).group(1)
        with open(f'attachments/{id}-#-{name}', "wb") as file:
            file.write(response.read())
    except:
        print(f"Failed to fetch attachment #{id}!")

print(f"Starting dump at {datetime.now()}")
Parallel(n_jobs=10)(delayed(fetch_bug)(id) for id in range(1, MAX_BUG_ID + 1))
print(f"Finished with bugs, dumping attachments at {datetime.now()}")
Parallel(n_jobs=10)(delayed(fetch_attachment)(id) for id in range(1, MAX_ATTACHMENT_ID + 1))
print(f"Finished with attachments at {datetime.now()}")

fetch("https://bugzilla.xfce.org/images/editbugs.png", "static/images/editbugs.png")
fetch("https://bugzilla.xfce.org/favicon.ico", "static/favicon.ico")
fetch("https://bugzilla.xfce.org/data/assets/21b8a893de66cbe8f722e158a53bc044.css?1588283465", "static/style/bz.css")
fetch("https://xfce.org/style/css.php?site=bugzilla", "static/style/bz.css", True)

# Append custom style
with open("static/style/bz.css", "a") as file:
    file.write("\n#bug-sidebar #sidebar-expand .field, #bug-sidebar #sidebar-expand .sidebar-item { display: block; }")
    file.write("\n.readonly { background: #e6461d; color: white; font-size: 150%; line-height: 2em; text-align: center; width: 100% !important; margin: 0 !important; }")
    file.write("\n.bz_bugitem .bz_bug_status_column span { background-color: initial; padding: 0; } .bz_bugitem .bz_resolution_column span { display: inline-block; width: 100%; text-align: center;}")
